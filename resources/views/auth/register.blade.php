<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label for="name" value="{{ __('Nombre') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label for="telefono" value="{{ __('Telefono') }}" />
                <x-jet-input id="telefono" class="block mt-1 w-full" type="number" name="telefono" :value="old('telefono')" required autofocus autocomplete="telefono" />
            </div>
            <div class="mt-4">
            <x-jet-label for="fecha" value="{{ __('Fecha Nacimiento') }}" />
            <x-jet-input id="fecha" type="date" name="fecha"  value="2020-01-01" required/>
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Contraseña') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirmar Contraseña') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                
                <input type="checkbox" id="cbox1" name="cbox1" value="1"  required >  <a class="underline text-sm text-gray-600 hover:text-gray-900" href="/">
                    <label>Terminos y Condiciones</label > </a> <br>
             
            </div>

            <div class="mt-4">
                
                <input type="checkbox" id="cbox2" name="cbox2" value="1"  required >  <a class="underline text-sm text-gray-600 hover:text-gray-900" href="/">
                    <label>Acepto Aviso de Privacidad</label > </a> <br>
             
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Ya estas Registrado?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Registro') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
<script>
  
</script>