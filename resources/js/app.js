require('./bootstrap');

require('moment');
require('inputmask');

import Vue from 'vue';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";
import Vuelidate from 'vuelidate';

// Vee Validate
import { ValidationProvider, ValidationObserver, setInteractionMode, localize } from 'vee-validate/dist/vee-validate.full.esm';
import es from 'vee-validate/dist/locale/es.json';

// Vee Validate Tipo de Validacion
setInteractionMode('lazy');

// Cambiar a Español Vee Validate
localize('es', es);

// Vee Validate Component
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

Vue.mixin({ methods: { route } });
Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);
Vue.use(Vuelidate);

Vue.config.baseurl = process.env.BASE_URL;


Vue.component("v-select", vSelect);

const app = document.getElementById('app');

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app);
