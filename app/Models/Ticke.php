<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticke extends Model
{
    use HasFactory;
    public $table = 'tickes';
    public $fillable = [
        'id',
        'monto',
        'tienda',
        'imagen_logo',
        'id_user',
     

    ];
    public $attributes = [
        "activo" => 1,
    ];


}
