<?php

namespace App\Http\Controllers;

use App\Http\Requests\TickeRequest;
use App\Models\Ticke;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class TickeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticke::orderBy('monto')->get();
        $data = $tickets->map(function ($ticket) {
            return [
                'id' => $ticket->id,
                'monto' => $ticket->monto,
                'tienda' => $ticket->tienda,
                'imagen_logo' => $ticket->imagen_logo,
                'activo' => $ticket->activo,
            ];
        });

        return Inertia::render('Tickets/Index', ['tickets' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Tickets/Form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = json_decode($request->form, true);

        if ($request->hasFile('imagen_logo') ) {
            $url_imagen = Storage::disk('public')->putFile('tickets', $request['imagen_logo']);
            $form['imagen_logo'] = $url_imagen;

        }

        Ticke::create($form);
        return redirect("tickets");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticke  $ticke
     * @return \Illuminate\Http\Response
     */
    public function show(Ticke $ticke)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticke  $ticke
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticke $ticke)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticke  $ticke
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticke $ticke)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticke  $ticke
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tict = Ticke::find($id);
        $tict->delete();
        return back();
        
    }

    
    public function ticketsActivo($id)
    {
        $dashboard = Ticke::find($id);
        $dashboard->activo = $dashboard->activo == 0 ? 1 : 0;
        $dashboard->save();
        return back();
    }

    public function terminos()
    {
        return Inertia::render('Tickets/Terminos');
   
    }

    public function avisos()
    {
        return Inertia::render('Tickets/Avisos');
    }
}
