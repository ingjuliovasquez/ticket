<?php

namespace Database\Factories;

use App\Models\Ticke;
use Illuminate\Database\Eloquent\Factories\Factory;

class TickeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ticke::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
